from helpers.file_helper import FileHelper


class AppService:

    def __init__(self):
        pass

    @staticmethod
    def get_server_licences():
        # print("Unix get cert info app started")
        try:
            file_helper = FileHelper()
            license_path = "/etc/ssl/certs/"

            if file_helper.check_path_exist(path=license_path):
                files = file_helper.get_files_in_path(directory_path=license_path)

                if files is not None:
                    # print("File List: " + str(files))

                    cert_log_file = open("cert_output.txt", "w")
                    cert_file_content = ""

                    for file_name in files:
                        if ".crt" in file_name:
                            print(file_name)
                            cert_file_content += file_name + '\n'

                            cert_info = file_helper.get_license_info_from_file(file_path=license_path + "/" + file_name)
                            cert_file_content += '\t\tNot Before: ' + cert_info['not_before'] + '\n'
                            cert_file_content += '\t\tNot After:  ' + cert_info['not_after'] + '\n'
                            print('\t\tNot Before: ' + cert_info['not_before'])
                            print('\t\tNot After:  ' + cert_info['not_after'])

                    cert_log_file.write(cert_file_content)
                    cert_log_file.close()
                else:
                    raise Exception("No files found in License path: " + license_path)
            else:
                raise Exception("License path not found: " + license_path)

            # print("Unix get cert info app is finished successfully.")
        except Exception as ex:
            raise Exception("An error occurred: " + ex.message)
