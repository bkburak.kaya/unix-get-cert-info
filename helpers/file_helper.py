import os
from os import listdir
from os.path import isfile, join


class FileHelper:

    def __init__(self):
        pass

    @staticmethod
    def check_path_exist(path):
        if os.path.exists(path):
            return True
        return False

    @staticmethod
    def get_files_in_path(directory_path):
        files = [f for f in listdir(directory_path) if isfile(join(directory_path, f))]

        if len(files) > 0:
            return files
        return None

    @staticmethod
    def get_license_info_from_file(file_path):
        cert_info = {'not_before': '', 'not_after': ''}
        cert_data = os.popen('openssl x509 -text -noout -in ' + file_path).read()

        for cert_line in cert_data.splitlines():
            if 'Not Before' in cert_line:
                cert_info['not_before'] = cert_line.replace('Not Before: ', '').lstrip()
            if 'Not After' in cert_line:
                cert_info['not_after'] = cert_line.replace('Not After : ', '').lstrip()

        return cert_info
