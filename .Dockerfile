FROM python:2

ADD services /
ADD helpers /
ADD app.py /

CMD [ "python", "app.py" ]